module SessionsHelper

  # Logs in the given user.
  # Places a temporary cookie on the user's broswer containing an encrypted version of the user's id, which allows us to retrieve the id on subsequent pages using session[:user_id]
  def log_in(user)
    session[:user_id] = user.id
  end

  #Remembers a user in a persistent session.
  def remember(user)
    user.remember
    # cookies to create permanent cookies for the user id and remember token as described.
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  # Returns the current logged-in user (if any)
  def current_user
    # retrieve the user from the temporary session if session[:user_id] exists, but otherwise look for cookies[:user_id] to retrieve the user corresponding to the persistent session.
    if (user_id = session[:user_id])  #if session of user id exists (while setting user id to session of user id).
      # find raises an exception if the user doesn't exist, but session[:user_id] will often be nil and find_by will return nil if the id is invalid
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id:user_id)
      if user && user.authenticated?(cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end

  # Forgets a persistent session.
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # Logs out the current user.
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end
end
