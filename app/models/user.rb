class User < ApplicationRecord

  has_many :fillable_forms

  before_save { email.downcase! }   #callback with "bang" method to modify the email attribute directly.

  attr_accessor :remember_token   #create an accessible attribute
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :name, presence: true, length: { maximum: 50 }
  validates :email, presence: true, length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false}
  validates :password, presence: true, length: { minimum: 6 }

  has_secure_password

  class << self
    #Returns the hash digest of the given string.
    def digest(string)
      # Creating our own password_digest attribute using has_secure_password
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                    BCrypt::Engine.cost
     BCrypt::Password.create(string, cost: cost)  #String to be hashed and the cost parameter that determines the computational cost to calculate the hash. Using a high cost makes it computationally intractable to use the hash to determine the original password, which is an important security precaution in the production environment, but in test we want the digest method to be asap.
    end

    # Returns a random token.
    def new_token
      SecureRandom.urlsafe_base64
    end

    # Remembers a user in the database for use in persistent sessions.
    def remember
      self.remember_token = User.new_token
      update_attribute(:remember_digest, User.digest(remember_token))   #updates the remember digest and bypasses the validations, which is necessary in this case because we don't have access to the user's password or confirmation.
    end

    # Returns true if the given token mathces the digest.
    def authenticated?(remember_token)
      return false if remember_digest.nil?
      BCrypt::Password.new(remember_digest).is_password?(remember_token)
    end
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end
end
