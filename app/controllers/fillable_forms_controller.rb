class FillableFormsController < ApplicationController

  skip_before_action :verify_authenticity_token

  def index
    @user = current_user
    @forms = @user.fillable_forms.all
  end

  def new
     @fillable_forms = FillableForm.new
  end

  def create
    @fillable_forms = current_user.fillable_forms.create(body: params[:post][:body])
    binding.pry
    if @fillable_forms.save
      redirect_to '/forms'
    else
      render 'new'
    end
  end

  # def show
  #   @fillable_forms = Post.find(params[:id])
  # end

  # def edit
  #   @fillable_forms = Post.find(params[:id])
  # end
  #
  # def update
  #   @fillable_forms = Post.find(params[:id])
  #
  #   if @fillable_forms.update(params[:post].permit(:body))
  #     redirect_to @fillable_forms
  #   else
  #     render 'edit'
  #   end
  # end

  # def destroy
  #   @fillable_forms = Post.find(params[:id])
  #   @fillable_forms.destroy
  #
  #   redirect_to forms_path
  # end

  private
    def fillable_forms_params
      params.require(:post).permit(:body)
    end
end
