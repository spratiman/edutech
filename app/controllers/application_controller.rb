class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper  #we arrange to make the helper functions available in the controllers as well
end
