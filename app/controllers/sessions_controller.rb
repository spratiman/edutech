class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      log_in @user
      params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)   # generates a remember token and saves its digest to the database. #params hash for submitted login form now includes a value based on the checkbox
      redirect_to @user  #Rails automatically converts this to the route for the user's dashboard
    else
      #Create an error mesage.
      flash.now[:danger] = "Invalid email/password combination"   #contents disappear as soon as there is an additional request unlike flash
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
