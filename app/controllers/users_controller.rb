class UsersController < ApplicationController

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)  # Not the final implementation!  #auxiliary method which returns an appropriate initialization hash and use it in place of params[:user]:
    if @user.save
      log_in @user  #automatically logs in new users
      flash[:success] = "Welcome to the EduTech App"  # Displays a temporary message.
      redirect_to @user   # Rails directly infers that could have used the equivalent redirect_to user_url(@user)
    else
      render 'new'
    end
  end

  private   # Since user_params will only be used internally by the Users controller and need not be exposed to external users via the web.

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
end
