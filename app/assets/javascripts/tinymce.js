tinymce.init({
    selector: ".tinymce",
    height: 300,
    plugins: [ 'table'],
    toolbar: "styleselect | fontsizeselect | bold italic | underline | alignleft | aligncenter | alignright | alignfull | fillable |",
    fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
    setup: function(editor) {
      editor.addButton('fillable', {
        text: 'Fillable Form',
        icon: false,
        onclick: function () {
          editor.insertContent('<form class="fillable-forms" style="resize:both;"><input type="text" placeholder="Answer Here"></input></form>');
        }
      });
    },
});
