require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  def setup   #automatically run before every test
    @base_title = 'EduTech'   #uses an instance variable to avoid repetition
  end

  test "should get home" do     #simply gets the home URL and verifies that the result is a success via assertion
    get root_path
    assert_response :success
    assert_select "title", "Home | #{@base_title}"  #testing the title tag
  end

  test "should get help" do   #simply gets the help URL and verifies that the result is a success via assertion
    get help_path
    assert_response :success
    assert_select "title", "Help | #{@base_title}"  #testing the title tag
  end

  test "should get about" do  #simply gets the about URL and verifies that the result is a success via assertion
    get about_path
    assert_response :success
    assert_select "title", "About | #{@base_title}"   #testing the title tag
  end

  test "should get contact" do  ##simply gets the contact URL and verifies that the result is a success via assertion
    get contact_path
    assert_response :success
    assert_select "title", "Contact | #{@base_title}"   #testing the title tag
  end

end
