require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout links" do  #1.Get the root path 2.Verify that the right page template is rendered 3.Check for the correct links to the Home, Help, About, and Contact pages.
    get root_path
    assert_template 'static_pages/home'   #verify that the home page is rendered using the correct view
    assert_select "a[href=?]", root_path, count: 2
    assert_select 'a[href=?]', helf_path
    assert_select "a[href=?]", about_path   # Rails automatically inserts the value of the about path in place of the question mark, thereby checking for an HTML tag of the form
    assert_select "a[href]", contact_path
  end
end
