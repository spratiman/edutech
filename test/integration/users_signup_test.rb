require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  test "invalid signup information" do
    get signup_path
    # arranges for a comparison between User.count before and after the contents inside the block. Recording the user count, posting the date, and verifying that the count is the same.
    assert_no_difference 'User.count' do
      post users_path, params: { user: {name: "",                               #in order to test the form submission
                                        email: "user@invalid",
                                        password:               "foo",
                                        password_confirmation:  "bar"}}
    end

  test "valid signup information" do
    get signup_path
    assert_difference "User.count", 1 do
      post users_path, params: {user: {name: "Example User",
                                       email: "user@example.com",
                                       password:               "password",
                                       password_confirmation:  "password"}}
    end
    follow_redirect!  #this simply arranges to follow the redirect after submission, resulting in a rendering of the 'user/show' template
    assert_template 'users/show'  #to check that a failed submission re-renders the new action.
    assert_not flash.FILL_IN
    assert is_logged_in?
  end
end
