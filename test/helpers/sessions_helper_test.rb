require 'test_helper'

class SessionHelperTest < ActionView::TestCase

#test sequence:
# 1. Define a user variale using the fixtures.
# 2. Call the remember method to remember the given user.
# 3. Varify that current_user is equal to the given user.

  def setup
    @user = users(:tyrion)
    remember(@user)
  end

  test "current_user returns right user when session is nil" do
    assert_equal @user, current_user  #if user && user.authenticated?(cookies[:remember_token])
    assert is_logged_in?
  end

  test "current_user returns nil when remember digest is wrong" do
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    assert_nil current_user
  end
end
