Rails.application.routes.draw do
  get 'sessions/new'

  get 'users/new'

  root 'static_pages#home'

  get '/help', to: 'static_pages#help', as: 'helf'

  get '/about', to: 'static_pages#about'

  get '/contact', to: 'static_pages#contact'

  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get '/forms', to: 'fillable_forms#index'
  get '/new_form', to: 'fillable_forms#new'
  post '/new_form', to: 'fillable_forms#create'

  resources :users  #endows the app with all the actions needed for a RESTful Users resource, along with a large number of named routes for generating user URLs.

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
